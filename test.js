var http = require('http');
var iconv = require('iconv-lite');
var cheerio = require('cheerio');



var myLog = function(str) {
    console.log(str)
}



var scrape_from_planet_site = function(cinema_id,_date) {
    return new Promise(function(resolve, reject) {

        //1010001 - Yes Planet Ayalon -  http://www.yesplanet.co.il/scheduleInfo?locationId=1010001&date=null&venueTypeId=0&hideSite=0

        var options = {
          // hostname: 'www.yesplanet.co.il',
          hostname: 'localhost',
          
          // path: "/scheduleInfo?locationId="+ cinema_id +"&date=null&venueTypeId=0&hideSite=0",
          path: "/movie_scrape/yes_planet_"+ cinema_id + ".html",
                    
          port: 80,
          method: 'GET',
          headers: {

            'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:51.0) Gecko/20100101 Firefox/51.0',
            'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language' : 'en-US,en;q=0.5',
            'Accept-Encoding' : 'gzip, deflate',
            'Cache-Control'     : 'no-cache',
            'Cookie'            : 'rbzid=VHBhMS9KVlJlWjVsTFBVdm9RVzJXSkgzTUlxQ3RQNXNqcVZKckpxMEFMVS9odldjSzZhOCtUNlFYM052UkZNdUJjNWxtWWJ2Z2Z5aG9lMEMwUGpzVWVvbHYydFRyZGJVNHNLTFdocyt3VGxIT0dLWHpZSG5UWFhaNmwwekY2bmw5dExaUjRSL1ZqSzRSeDdLa2lObit5MnFmRmg1MVZjUEowTE5ScVBIcy8vamVTK2k4WWFCUDQ5R2JSaHJUalF2aForWXA2V0FvOFd3UXQrYktNNGswWjN4OWNHUEtCekIvRXdETndRbHVIaz1AQEAxMThAQEAtMTQ4MTQ4MTQ2ODA-'
          }
        };

        http.get(options, function(res) {            
            try {
                res.pipe(iconv.decodeStream("UTF-8")).collect(function(err, decodedBody) {
                    try {

                        var $ = cheerio.load(decodedBody);
                        
                        var body = $('body')

                        var combinedScript = 'var document = {"documentElement": {"scrollLeft":1,"scrollTop":1} }; ' +  
                          'var screen = {  }; ' +
                          'var navigator = {  }; ' +
                          'var window = {  "document": document , "screen" : screen , "navigator" : navigator}; '
                        ;

						            body.find('script').each( function(index, element) {
                                combinedScript += '\n\n\n\n\n\n' + $(this).text()
                                combinedScript += 'var rbzns = window.rbzns; var winsocks = window.winsocks' // beacue window exposes to global everything that is set on it
                        })




                         //myLog(combinedScript)
                         combinedScript += "; var f = function() {return window};f()"
                         var ret = eval(combinedScript)
                         myLog(ret)

                        resolve(JSON.stringify("[]"))

                    } catch (e) {
                        myLog("Error 1 - Func:scrape_from_planet_site " + e)   
                    }
                })
            } catch (exception) {
                myLog("Error 2 - Func:scrape_from_planet_site " + exception)   
            }
        })

    })
}

scrape_from_planet_site(1010001,new Date())