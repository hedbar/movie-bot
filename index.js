

'use strict';
var http = require('http');
var https = require('https');
var zlib = require('zlib');
var vm = require('vm');
var iconv = require('iconv-lite');
var cheerio = require('cheerio');
var cookie = require('cookie');




var dynamo,express, AWS,dynamodb_doc;
var NO_CACHE_CONSTANT = "" //put something to kill cache on get - refreshes the cache

var local = false;

try {
    dynamodb_doc = require('dynamodb-doc');
} catch(e) {
    console.error("dynamodb-doc is not found => we are local");
    local = true
}


if (local) {

    process.env.NODE_PATH = "local_node_modules";
    require('module').Module._initPaths();

    // comment when running install
    express = require('express');
    AWS = require("aws-sdk");
    AWS.config.update({
      accessKeyId: "fakeAccessKey",
      secretAccessKey: "fakeSecretAccessKey",
      region: "us-west-2",
      endpoint: "http://localhost:8000"
    });

    dynamo = new AWS.DynamoDB.DocumentClient()
    dynamo.putItem = dynamo.put
    dynamo.getItem = dynamo.get
    dynamo.deleteItem = dynamo.delete
} else {
    dynamo = new dynamodb_doc.DynamoDB();
}




var runningLog = "";
var myLog = function(str) {
    console.log(str)
    runningLog += "\n<br/>" + str
}

var get_from_cache = function(area,_date) {

    myLog("Func:get_from_cache_dynamo")
    return new Promise(function(resolve, reject) {

        var params = {
            TableName: "Data",
            Key:{
                "date": _date.getFullYear() + "_" + (_date.getMonth()+1) + "_" + _date.getDate(),
                "region": "" + area + NO_CACHE_CONSTANT
            }
        };

        myLog("Params : " + JSON.stringify(params))

        try {
            dynamo.getItem(params, function(err, data) {
                try {
                    if (err || !data["Item"]) {
                            if (err) {
                                myLog("Error in dynamo.getItem err=" + err )
                            } else {
                                myLog("Element (" + area +"," + _date + ") Not found")
                            }
                            reject()
                    } else {
                        myLog("Element (" + area +"," + _date + ") Found")
                        resolve(data.Item.info)
                    }
                } catch (e) {
                    myLog("Internal Exception in dynamo.getItem: " + e)
                }
            });
        } catch (e) {
                myLog("Sync Exception in dynamo.getItem: " + e)
        }

    })

}


var save_to_cache = function(area,_date,value) {
    myLog("Func:save_to_cache_dynamo")
    var params = {
        TableName: "Data",
        Item:{
            "date": _date.getFullYear() + "_" + (_date.getMonth()+1) + "_" + _date.getDate(),
            "region": "" + area,
            "info": value
        }
    };

    try {
        dynamo.putItem(params, function(err, data) {
            try {
                if (err) {
                    myLog("Error: Element (" + area +"," + _date + ") not set. Error: " + err)
                    myLog("Tried to access table :" + params.TableName);
                } else {
                    myLog("Element (" + area +"," + _date + ") set")
                }
            }  catch (e) {
                myLog("Internal Exception in dynamo.putItem: " + e)
            }
        });
    } catch (e) {
            myLog("Sync Exception in dynamo.putItem: " + e)
    }

}



// Each resolved value is of the following format:
// [
//    {
//       "hour":"17:00",
//       "cinemas":[
//          {
//             "cinema":"CINEMA_NAME",
//             "movies":[
//                {
//                   "movie":"MOVIE_NAME"
//                },
//                ...
//             ]
//          }
//       ]
//    },
//    ...
// ]

var planet_anti_bot_cookie = '';
var get_planet_anti_bot_cookie = function (decodedBody, options) {

    return new Promise(function(resolve, reject ) {

        var noop = function() {};
        var sandbox = {
            document: {
                documentElement: ''
            },
            screen: {
                width: 1920,
                height: 1080
            },
            navigator: {plugins:{}},
            frames: '',
            length: 0,
            pageYOffset: 0,
            pageXOffset: 0,
            innerWidth: 1920,
            innerHeight: 974,
            outerWidth: 1920,
            outerHeight: 1040,
            Image: noop,
            status: '',
            name: '',
            setTimeout: setTimeout,
            location: {
                reload: noop
            }
        };
        sandbox.window = sandbox.self = sandbox;

        Object.defineProperty( sandbox.document, 'cookie', {

            set: function ( val ) {
                var cookies = cookie.parse( val );
                resolve( cookies.rbzid );
            }

        } );

        var context = new vm.createContext(sandbox);
        var $ = cheerio.load(decodedBody);

        $( 'script' ).each( function () {
            vm.runInContext( $(this).text(), context, { displayErrors: true } );
        } );

        if ( options.timeout ) {
            setTimeout( reject, options.timeout );
        }

    } );

};

var scrape_from_planet_site = function(cinema_id,_date) {
    myLog("Func:scrape_from_planet_site")
    return new Promise(function(resolve, reject) {

        //1010001 - Yes Planet Ayalon -  http://www.yesplanet.co.il/scheduleInfo?locationId=1010001&date=null&venueTypeId=0&hideSite=0

        var options = {
          hostname: 'www.yesplanet.co.il',
          // hostname: 'localhost',

          path: "/scheduleInfo?locationId="+ cinema_id +"&date=null&venueTypeId=0&hideSite=0",
          // path: "/movie_scrape/yes_planet_"+ cinema_id + ".htm",

          port: 80,
          method: 'GET',
          headers: {

            'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:51.0) Gecko/20100101 Firefox/51.0',
            'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Language' : 'en-US,en;q=0.5',
            'Accept-Encoding' : 'gzip, deflate',
            'Cache-Control'     : 'no-cache',
            // 'Cookie'            : 'rbzid=VHBhMS9KVlJlWjVsTFBVdm9RVzJXSkgzTUlxQ3RQNXNqcVZKckpxMEFMVS9odldjSzZhOCtUNlFYM052UkZNdUJjNWxtWWJ2Z2Z5aG9lMEMwUGpzVWVvbHYydFRyZGJVNHNLTFdocyt3VGxIT0dLWHpZSG5UWFhaNmwwekY2bmw5dExaUjRSL1ZqSzRSeDdLa2lObit5MnFmRmg1MVZjUEowTE5ScVBIcy8vamVTK2k4WWFCUDQ5R2JSaHJUalF2aForWXA2V0FvOFd3UXQrYktNNGswWjN4OWNHUEtCekIvRXdETndRbHVIaz1AQEAxMThAQEAtMTQ4MTQ4MTQ2ODA-'
            // 'Cookie'            : '_ga=GA1.3.756404261.1482918239; default_subsiteid=1010003_5; rbzid=UTRKbUd3MDU4bFBIcnlmVlI5L2xkRUxUbDZacWcwejArempBS0xrcHJSQk5QMG9odEtiNndkSGZHYnRvdlpmYWw0NFVBZURMVEZvZUx4Wk05QkJTcXVxR3RXa0llZ2Z5RTFuM215NUdjWGlNdllwaUFrQnZEcjJyNEZsWldBOXFYeDhGMmtiT2Jsek05MVRCcGpqaHhhT1grNUJqU09pOWVuYnZuK0FEMURnPUBAQDYyQEBALTE0ODE0ODE0Njgw; JSESSIONID=CD7AD1317A100524E3E8349FF2CBBF34'
          }
        };

        if (planet_anti_bot_cookie) {
            myLog("Func:scrape_from_planet_site found rbzid cookie");

            if (options.headers.Cookie) {
                options.headers.Cookie += ';';
            } else {
                options.headers.Cookie = '';
            }
            options.headers.Cookie += 'rbzid=' + planet_anti_bot_cookie;


        }

        myLog("Calling " + options.hostname + options.path)
        http.get(options, function(res) {
            myLog("Func:scrape_from_planet_site http.get returned ")
            try {
                var output = res;

                if (res.headers['content-encoding']) {
                    output = res.pipe(zlib.createUnzip());
                }
                output.pipe(iconv.decodeStream("UTF-8")).collect(function(err, decodedBody) {
                    myLog ("Response only in console.log")
                    console.log("Func:scrape_from_planet_site: Response : " + decodedBody)
                    try {

                        if (!planet_anti_bot_cookie) {
                            myLog("Func:scrape_from_planet_site loading rbzid cookie");
                            return get_planet_anti_bot_cookie(decodedBody, {timeout: 1000}).then(function(cookie) {
                                if (cookie) {
                                    planet_anti_bot_cookie = cookie;
                                    resolve(scrape_from_planet_site(cinema_id,_date));
                                }
                            }).catch( function () {
                                myLog("Func:scrape_from_planet_site cookie error")
                                resolve('[{"hour":"00:00", "cinemas":[{"cinema":"Yes Planet scraping failed","movies":[] }] }]')
                            });
                        } else {
                            // Ensure a new cookie will be calculated on the
                            // next request
                            planet_anti_bot_cookie = '';
                        }

                        var $ = cheerio.load(decodedBody);
                        myLog("Func:scrape_from_planet_site cheerio loaded ")

                        var currentMovie = '',currentHour = ''
                        var hours = {}, hoursArr = []
                        var cinemaName = "";
                        try {
                            cinemaName = $('.locationName').text().trim() + ' :orig'
                        } catch (e) {
                            resolve('[{"hour":"00:00", "cinemas":[{"cinema":"Yes Planet scraping failed","movies":[] }] }]')
                        }
                        var rootDiv = $('.scheduleInfoTable');
                        rootDiv.find('a.featureLink, a.presentationLink').each( function(index, element) {
                            if ($(this).attr('class') == 'featureLink') {
                                currentMovie = $(this).text().trim()
                                myLog ("Movie name: " + $(this).text() + ", after trim: " + currentMovie)
                            } else {
                                currentHour = $(this).text().trim().substr(0,5)
                                if (hours[currentHour]) {
                                    hours[currentHour].cinemas[0].movies.push({'movie':currentMovie})
                                } else {
                                    var newFoundHour = {'hour':currentHour,'cinemas':[{'cinema':cinemaName,movies:[{'movie':currentMovie}]}]}
                                    hours[currentHour] = newFoundHour
                                    hoursArr.push(newFoundHour)
                                }
                            }
                        })
                        resolve(JSON.stringify(hoursArr))

                        // resolve('[{"hour":"22:22", "cinemas":[{"cinema":"'+ cinema_id+'","movies":[{"movie":"movie_name"}]}] }]')

                    } catch (e) {
                        myLog("Error 1 - Func:scrape_from_planet_site " + e)
                        resolve('[{"hour":"00:00", "cinemas":[{"cinema":"Yes Planet scraping failed","movies":[] }] }]')
                    }
                })
            } catch (exception) {
                myLog("Error 2 - Func:scrape_from_planet_site " + exception)
                resolve('[{"hour":"00:00", "cinemas":[{"cinema":"Yes Planet scraping failed","movies":[] }] }]')
            }
        })

    })
}


var format_date_yyyy_mm_dd = function(_date) {
    var month_str = ((_date.getMonth()+1)<10) ? '0'+(_date.getMonth()+1) : ''+ (_date.getMonth()+1)
    var date_str = (_date.getDate()<10) ? '0'+ _date.getDate() : ''+ _date.getDate()
    var ret = _date.getFullYear() + '-' + month_str + '-' + date_str
    return ret
}


var format_hour_mm_dd = function(_date) {
    var hour_str = (_date.getHours()<10) ? '0'+(_date.getHours()) : ''+ (_date.getHours())
    var minute_str = (_date.getMinutes()<10) ? '0'+ _date.getMinutes() : ''+ _date.getMinutes()
    var ret = hour_str + ':' + minute_str
    return ret
}



var RAV_HEN_theatre_name = {
    1071: "רב חן דיזינגוף",
    1058:  "רב חן גבעתיים",
    1061:  "רב חן שבעת הכוכבים"
}


var scrape_from_rav_hen = function(cinema_id, _date) {
    myLog("Func:scrape_from_rav_hen")
    return new Promise(function(resolve, reject) {
        var data = [];
        var date_str = format_date_yyyy_mm_dd(_date)
        var is_debug = false
        var options = {
            hostname: (is_debug)?'localhost':'www.rav-hen.co.il',

            path: (is_debug)? "/rav_hen.json" :
                "/rh/data-api-service/v1/quickbook/10104/film-events/in-cinema/"+cinema_id+"/at-date/"+date_str+"?attr=&lang=he_IL"
            ,

            port: (is_debug)?80:443,
            method: 'GET',
            headers: {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                'Accept-Encoding': 'deflate'
            }
        };

        myLog("Calling " + options.hostname + options.path)
        var callFunc = (is_debug)?http.get:https.get;
        callFunc(options, function(res) {

          try {
              res.pipe(iconv.decodeStream("UTF-8")).collect(function(err, decodedBody) {
                myLog("Func:scrape_from_rav_hen http.get returned " + decodedBody)
                
                var jsonObj = JSON.parse(decodedBody)
                var currentHour = {"hour":"-1"};
                var currentCinema, currentMovie = {};
                  
                
                jsonObj.body.events.forEach(element => {
                    var hour = new Date(element.eventDateTime)
                    var hourStr = format_hour_mm_dd(hour)
                    if (hourStr != currentHour.hour) {
                        currentHour = {"hour":hourStr, cinemas:[ {cinema: RAV_HEN_theatre_name[cinema_id] , movies: [] } ] }
                        data.push(currentHour);                        
                    }                    
                    
                    var movie = jsonObj.body.films.find(function(film) { return film.id == element.filmId })
                    currentHour.cinemas[0].movies.push ( {"movie":  movie.name } )
                    
                });

                
                resolve(JSON.stringify(data))
                
              })
          } catch (exception) {
              myLog("Error 2 - Func:scrape_from_rav_hen " + exception)
              resolve('[{"hour":"00:00", "cinemas":[{"cinema":"Cinematec scraping failed","movies":[] }] }]')
          }

      }).on('error', (e) => {
        myLog('Func:scrape_from_rav_hen http.get returned error: ' + e);
        resolve('[{"hour":"00:00", "cinemas":[{"cinema":"Cinematec scraping failed","movies":[] }] }]')
      })
    })
}


var scrape_from_cinematec = function(cinema_id, _date) {
    myLog("Func:scrape_from_cinematec")
    return new Promise(function(resolve, reject) {
        var data = [];
        var date_str = _date.getFullYear() +"-"+(_date.getMonth()+1)+"-"+_date.getDate()
        var options = {
            hostname: 'www.cinema.co.il',
            // hostname: 'localhost',

            path: "/wp-content/themes/cinematheque/ajax_data.php?action=get_cal_data&ids=&date="+date_str+"&lang=he&tc=1546670905404",
            // path: "/cinematech.html",


            port: 443,
            method: 'GET',
            headers: {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                'Accept-Encoding': 'deflate'
            }
        };

        https.get(options, function(res) {

          myLog("Func:scrape_from_cinematec http.get returned ")
          try {
              res.pipe(iconv.decodeStream("UTF-8")).collect(function(err, decodedBody) {

                  var $ = cheerio.load(decodedBody);
                  myLog("Func:scrape_from_cinematec cheerio loaded ")

                  var currentHour = {"hour":"-1"};
                  var currentCinema, currentMovie = {};
                  var rootDiv = $('.swiper-wrapper');
                //   myLog(rootDiv.text())
                  
                  rootDiv.find('.showlist_item').each(function(index, element) {
                      var hourStr =  $(this).find('.time_cat').contents()
                            .filter(function(key,val) { return val.type == 'text'; } ).text().trim()
                      if (hourStr != currentHour.hour) {
                        currentHour = {"hour":hourStr, cinemas:[ {cinema:"סינמטק" , movies: [] } ] }
                        data.push(currentHour);
                      }
                      var movie_name = $(this).find('.item_detail').find('.s2').first().text().trim();
                      currentHour.cinemas[0].movies.push ( {"movie":  movie_name } )
                  })
                  resolve(JSON.stringify(data))

              })
          } catch (exception) {
              myLog("Error 2 - Func:scrape_from_cinematec " + exception)
              resolve('[{"hour":"00:00", "cinemas":[{"cinema":"Cinematec scraping failed","movies":[] }] }]')
          }

      }).on('error', (e) => {
        myLog('Func:scrape_from_cinematec http.get returned error: ' + e);
        resolve('[{"hour":"00:00", "cinemas":[{"cinema":"Cinematec scraping failed","movies":[] }] }]')
      })
    })
}

var LEV_theatre_name = {
    1156: "מנדרין" ,
    1150: "לב דיזינגוף סנטר" , // 1020001
    1154: "לב דניאל" // 1020006
}

var scrape_from_Lev_site = function(cinema_id,_date) {
    myLog("Func:scrape_from_LEV_site")
    return new Promise(function(resolve, reject) {



        var data = [];        
        var date_str = _date.getFullYear() +"-"+(_date.getMonth()+1)+"-"+_date.getDate()

        var options = {
          hostname: 'lev.co.il',
        //   hostname: 'localhost',

          path : "/wp-content/themes/lev/ajax_data.php?clang=he&action=movie_on_location&loc="+cinema_id+"&date="+date_str+"&_=1546034515816",
        //   path: "/lev_mandarin.html",


          port: 443,
          method: 'GET',
          headers: {
            'User-Agent' : 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0',
            'Accept' : 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
            'Accept-Encoding' : 'deflate'
          }
        };

        myLog("Calling " + options.hostname + options.path)



        https.get(options, function(res) {
            myLog("Func:scrape_from_Lev_site http.get returned ")
            try {
                res.pipe(iconv.decodeStream("UTF-8")).collect(function(err, decodedBody) {
                    // myLog("Response From Lev : " + decodedBody)

                    try {                        
                        var $ = cheerio.load(decodedBody);
                        myLog("Func:scrape_from_Lev_site cheerio loaded ")

                        var currentHour = {"hour":"-1"};
                        var currentCinema, currentMovie = {};
                        $('span').each(function(index, element) {
                            var hourStr = $(this).text();
                            if (hourStr != currentHour.hour) {
                              currentHour = {"hour":hourStr, cinemas:[ {cinema:LEV_theatre_name[cinema_id] , movies: [] } ] }
                              data.push(currentHour);
                            }
                            currentHour.cinemas[0].movies.push ( 
                                {"movie":   $(this.parent).contents()[0].data
                            } )
                        })
                        resolve(JSON.stringify(data))
                    } catch (e) {
                        myLog("Error 1 - Func:scrape_from_Lev_site " + e)
                        resolve('[{"hour":"00:00", "cinemas":[{"cinema":"Lev scraping failed","movies":[] }] }]')
                    }

                    //resolve('[{"hour":"22:22", "cinemas":[{"cinema":"'+ cinema_id+'","movies":[{"movie":"movie_name"}]}] }]')

                })
            } catch (exception) {
                myLog("Error 2 - Func:scrape_from_Lev_site " + exception)
                resolve('[{"hour":"00:00", "cinemas":[{"cinema":"Lev scraping failed","movies":[] }] }]')
            }
        })

    })
}



var cinema_city_theatre_name = {
    1170: "סינמה סיטי גלילות" ,
    1173: "סינמה סיטי ראשלצ"
}

var scrape_from_cinema_city_site = function(cinema_id,_date) {
    myLog("Func:scrape_from_cinema_city_site")
    return new Promise(function(resolve, reject) {

        var data = [];        
        var date_str = _date.getFullYear() +"-"+(_date.getMonth()+1)+"-"+_date.getDate()

        var is_debug = false
        var options = {
            hostname: (is_debug)?'localhost':'www.cinema-city.co.il',

            path: (is_debug)? "/cinema-city.html" :
                "/Timehour?theathereid="+cinema_id
            ,

            port: (is_debug)?80:443,
            method: 'GET',
            headers: {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                'Accept-Encoding': 'deflate'
            }
        };

        myLog("Calling " + options.hostname + options.path)
        var callFunc = (is_debug)?http.get:https.get;
        callFunc(options, function(res) {
            myLog("Func:scrape_from_cinema_city_site http.get returned ")
            try {
                res.pipe(iconv.decodeStream("UTF-8")).collect(function(err, decodedBody) {
                    // console.log("Response From cinema_city : " + decodedBody)

                    try {                        
                        var $ = cheerio.load(decodedBody);
                        myLog("Func:scrape_from_cinema_city_site cheerio loaded ")

                        var currentHour = {"hour":"-1"};
                        var currentCinema, currentMovie = {};


                        $('.time-wrap').each(function(index, element) {
                            var hourStr = $(this).find('.movie-hour').text();
                            if (hourStr != currentHour.hour) {
                              currentHour = {"hour":hourStr, cinemas:[ {cinema:cinema_city_theatre_name[cinema_id] , movies: [] } ] }
                              data.push(currentHour);
                            }
                            var movie_name = $(this).find('.img-fluid')[0].attribs['alt'] 
                            currentHour.cinemas[0].movies.push ( 
                                {"movie": movie_name} )
                        })
                        resolve(JSON.stringify(data))
                    } catch (e) {
                        myLog("Error 1 - Func:scrape_from_cinema_city_site " + e)
                        resolve('[{"hour":"00:00", "cinemas":[{"cinema":"cinema_city scraping failed","movies":[] }] }]')
                    }

                    //resolve('[{"hour":"22:22", "cinemas":[{"cinema":"'+ cinema_id+'","movies":[{"movie":"movie_name"}]}] }]')

                })
            } catch (exception) {
                myLog("Error 2 - Func:scrape_from_cinema_city_site " + exception)
                resolve('[{"hour":"00:00", "cinemas":[{"cinema":"cinema_city scraping failed","movies":[] }] }]')
            }
        })

    })
}


var seret_il_theatre_name = {
    152: "מוזיאון טיקוטין" ,
    181: "סינמטק 2 חיפה",
    23: "סינמה קפה",
    27: "יס פלאנט",
    194: "גרנד קניון",

    75: "מוזיאון תא",
    61: "יס פלאנט - אילון",
    162: "יס פלאנט ראשון לציון"
}


var scrape_from_SeretCoIL_site = function(cinema_id,_date) {
    myLog("Func:scrape_from_seret_il_site")
    return new Promise(function(resolve, reject) {

        var data = [];        
        var date_str = (_date.getDate()+3) % 7 +1 

        var is_debug = false
        var options = {
            hostname: (is_debug)?'localhost':'www.seret.co.il',

            path: (is_debug)? "/seret_il.html" :
                "/movies/movieTableAjax.asp?f_tid="+cinema_id+"&d="+date_str+"&st=false"
            ,

            port: (is_debug)?80:443,
            method: 'GET',
            headers: {
                'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:50.0) Gecko/20100101 Firefox/50.0',
                'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
                'Accept-Encoding': 'deflate'
            }
        };

        myLog("Calling " + options.hostname + options.path)
        var callFunc = (is_debug)?http.get:https.get;
        callFunc(options, function(res) {
            myLog("Func:scrape_from_seret_il_site http.get returned ")
            try {
                res.pipe(iconv.decodeStream("win1255")).collect(function(err, decodedBody) {
                    // console.log("Response From seret_il : " + decodedBody)

                    try {                        
                        var $ = cheerio.load(decodedBody);
                        myLog("Func:scrape_from_seret_il_site cheerio loaded ")

                        var currentHour = {"hour":"-1"};
                        var currentCinema, currentMovie = {};


                        $('.textGrayStrong18').each(function(index, element) {
                            var hourStr = $(this).text().trim();
                            if (hourStr != currentHour.hour) {
                              currentHour = {"hour":hourStr, cinemas:[ {cinema:seret_il_theatre_name[cinema_id] , movies: [] } ] }
                              data.push(currentHour);
                            }
                            var movie_name = $(this).next().text().trim()
                            currentHour.cinemas[0].movies.push ( 
                                {"movie": movie_name} )
                        })
                        resolve(JSON.stringify(data))
                    } catch (e) {
                        myLog("Error 1 - Func:scrape_from_seret_il_site " + e)
                        resolve('[{"hour":"00:00", "cinemas":[{"cinema":"seret_il scraping failed","movies":[] }] }]')
                    }

                    //resolve('[{"hour":"22:22", "cinemas":[{"cinema":"'+ cinema_id+'","movies":[{"movie":"movie_name"}]}] }]')

                })
            } catch (exception) {
                myLog("Error 2 - Func:scrape_from_seret_il_site " + exception)
                resolve('[{"hour":"00:00", "cinemas":[{"cinema":"seret_il scraping failed","movies":[] }] }]')
            }
        })

    })
}


var getJSON = function(area,_date,scrapingFunc) {
    var key = ("#" + scrapingFunc)
    key = key.match(/myLog.*/)[0] 
    myLog ("[getJSON] promise: " + key)
    return new Promise(function(resolve, reject) {

		get_from_cache(area,_date)
		.then( function (value) { 
            myLog ("[getJSON] promise cache resolved : " + key)
            resolve(value) } )
        .catch (function (value) {
            myLog("Not found in cache")
            scrapingFunc(area,_date).
                then(function (value) {
                    save_to_cache(area,_date,value)
                    myLog ("[getJSON] promise scraping resolved : " + key)
                    resolve(value)
                }).
                catch (function (err) {
                    myLog ("[getJSON] promise scraping failed : " + key + "\n\n" + err)
                    resolve({})
                })
        })
    })
}


var getILDate = function(gmtDate) {
	var d1 = new Date(gmtDate.getTime() + gmtDate.getTimezoneOffset()*60*1000 + 120*60*1000 /* IL = GMT+2 */)
	return d1;
}


var to_time_str = function(h,m) {
    var time =
        ((h < 10)? "0" + h : ""+ h)
         + ":" +
        ((m < 10)? "0" + m : ""+  m)
    ;
    return time;
}



var hour_filter = function(jsonObj,d,hours_range) {
    var ret = []
    var _from_hour = to_time_str(d.getHours(),d.getMinutes())
    var to_hour = to_time_str(d.getHours()+hours_range,d.getMinutes())

    myLog("hour_filter: _from_hour=" + _from_hour + " to_hour=" + to_hour +" d=" +  d )

    jsonObj.forEach(function(o){
      if (o.hour > _from_hour && o.hour < to_hour) {
        ret.push(o)
      }
    });
    return ret;
}



var render_html_page = function(jsonObj,is_qa_mode) {
    var page =
    "<html>" +
    "<head><style>" +
        "table, th, td { border: 1px solid black; }"+
        ".clickable_links {  font-size: 40px; }" +
    "</style></head>"+
    "<body dir='rtl' >"
    if (is_qa_mode) {
        page += "<p>" + runningLog + "</p>"
    }

    page += "<p class='clickable_links' >"
    // page += " <a href='?area=TA'> תל-אביב </a>  <a href='?area=TA&range=24'> (כל היום) </a>     <br/>"
    page += " <a href='?area=TA_Plus'> תל-אביב רבתי </a>  <a href='?area=TA_Plus&range=24'> (כל היום) </a>     <br/>"
    page += " <a href='?area=haifa'> חיפה </a>  <a href='?area=haifa&range=24'> (כל היום) </a>  <br/>"
    page += "</p>"

    page += render_hours(jsonObj)
    page += "</body></html>"
    return page

}

var render_hours = function(jsonObj) {
    var page = "";
    if (jsonObj) {
        page += "<table>"
        jsonObj.forEach(function(o){
            page += "<tr>"
            page += "<td>" + o.hour + "</td>";
            page += "<td>" + render_cinemas(o.cinemas) + "</td>";
            page += "</tr>"
        })

        page += "</table>"
    }
    return page
}

var render_cinemas = function(jsonObj) {
    var page = "";
    if (jsonObj) {
        page += "<table>"
        jsonObj.forEach(function(o){
            page += "<tr>"
            // if (o.movies.length == 1) {
            //     page += "<td>" + o.cinema + " : " + o.movies[0].movie + "</td>";
            // } else {
                page += "<td>" + o.cinema + "</td>";
                page += "<td>" + render_movies(o.movies) + "</td>";
                page += "</tr>"
            //}
        })

        page += "</table>"
    }
    return page
}

var render_movies = function(jsonObj) {
    var page = "";
    if (jsonObj) {
        jsonObj.forEach(function(o){
            page += o.movie + "<br/>";
        })
    }
    return page
}

var merge = function(o,o1) {
    var ret = o
    if (!o) {
        ret = o1
    } else {
        var tmp = o.concat(o1)
        tmp.sort(function(a, b) {return (a.hour>b.hour)?1:-1} ) // sort is needed internally because the hour comparison is depndent on it

        ret = []
        tmp.forEach(function(o){
            if (ret.length == 0) {
                ret.push(o)
            } else if (o.hour != ret[ret.length-1].hour) {
                ret.push(o)
            } else {
                ret[ret.length-1].cinemas = ret[ret.length-1].cinemas.concat(o.cinemas)
            }
        })
    }

    return ret
}




exports.handler = (event, context, callback) => {
    runningLog = "" + new Date();
    myLog('Received event:', JSON.stringify(event, null, 2));

    var hours_range = 3;
    var gmtDate = new Date();
    if (event.queryStringParameters) {
        if (event.queryStringParameters["hour"]) {
            gmtDate.setHours(event.queryStringParameters.hour)
        }
        if (event.queryStringParameters["minutes"]) {
            gmtDate.setMinutes(event.queryStringParameters.minutes)
        }
        if (event.queryStringParameters["range"]) {
            hours_range = Number(event.queryStringParameters.range)
        }
    }

    if (event.queryStringParameters && event.queryStringParameters["reset_cache"]) {
        NO_CACHE_CONSTANT = '#'
    } else {
        NO_CACHE_CONSTANT = ''
    }

    var regionArr = []

    if (event.queryStringParameters && event.queryStringParameters["area"] && event.queryStringParameters.area == "test") {
        regionArr = [
            getJSON(152,getILDate(gmtDate),scrape_from_SeretCoIL_site)
        ]

    } else if (event.queryStringParameters && event.queryStringParameters["area"] && event.queryStringParameters.area == "haifa") {
        regionArr = [

            getJSON(152,getILDate(gmtDate),scrape_from_SeretCoIL_site),
            getJSON(181,getILDate(gmtDate),scrape_from_SeretCoIL_site),
            getJSON(23,getILDate(gmtDate),scrape_from_SeretCoIL_site),
            getJSON(27,getILDate(gmtDate),scrape_from_SeretCoIL_site),
            getJSON(194,getILDate(gmtDate),scrape_from_SeretCoIL_site)

        ]
    } else if (event.queryStringParameters && event.queryStringParameters["area"] ) {
        regionArr = [

        getJSON(75,getILDate(gmtDate),scrape_from_SeretCoIL_site),
        getJSON(61,getILDate(gmtDate),scrape_from_SeretCoIL_site),
        getJSON(162,getILDate(gmtDate),scrape_from_SeretCoIL_site),
        
        getJSON(1173,getILDate(gmtDate),scrape_from_cinema_city_site),
        getJSON(1170,getILDate(gmtDate),scrape_from_cinema_city_site),

        getJSON(1071,getILDate(gmtDate),scrape_from_rav_hen),
        getJSON(1058,getILDate(gmtDate),scrape_from_rav_hen),
        getJSON(1061,getILDate(gmtDate),scrape_from_rav_hen),

        getJSON(1150,getILDate(gmtDate),scrape_from_Lev_site), getJSON(1154,getILDate(gmtDate),scrape_from_Lev_site),
        getJSON(1156,getILDate(gmtDate),scrape_from_Lev_site),

        getJSON(1,getILDate(gmtDate),scrape_from_cinematec)
        ]
    }



// Each resolved value is of the following format:
// [
//    {
//       "hour":"17:00",
//       "cinemas":[
//          {
//             "cinema":"CINEMA_NAME",
//             "movies":[
//                {
//                   "movie":"MOVIE_NAME"
//                },
//                ...
//             ]
//          }
//       ]
//    },
//    ...
// ]





  Promise.all(regionArr).
  then(function(jsonArr) {
    var jsonObj = null;
    jsonArr.forEach(function(o){
        jsonObj = merge(jsonObj,JSON.parse(o))
    })


    jsonObj = hour_filter(jsonObj,getILDate(gmtDate),hours_range);

    jsonObj.sort(function(a, b) {return (a.hour>b.hour)?1:-1} )

    jsonObj.forEach(function(o) {
      o.cinemas.sort(function(a, b) {
        return (a.cinema>b.cinema)?1:-1
        })
    })


    callback(null, //error
     {
        statusCode:200,
        body: render_html_page(jsonObj, event.queryStringParameters && event.queryStringParameters["qa"] ),
        headers: {
            'Content-Type': 'text/html; charset=UTF-8',
            'Cache-Control': 'no-cache',
        },
    });

  })
  .catch(function(err) {
    myLog("Promise.all : " + err)
  })



};



if (local) {
    const app = express();

    app.get('/prod/getMovies', (req, res) => {
        var queryStringParameters = {qa:"true"}
        if (req.query.hour) {
            queryStringParameters.hour = req.query.hour
        }
        if (req.query.minutes) {
            queryStringParameters.minutes = req.query.minutes
        }
        if (req.query.area) {
            queryStringParameters.area = req.query.area
        }
        if (req.query.range) {
            queryStringParameters.range = req.query.range
        }
        if (req.query.reset_cache) {
            queryStringParameters.reset_cache = req.query.reset_cache
        }
        exports.handler(
            {queryStringParameters:queryStringParameters}, // event
            {}, // context
            function (error,result) {
                if (error) {
                    console.log("Callback called with an error:" + error)
                    exit()
                }
                else {
                    res
                    .status(result.statusCode)
                    .send(result.body);
                }
            }
        )
    });


    app.get('/local_delete_item', (req, res) => {

        myLog("Func:local_delete_item")
        if (req.query.area) {
            var area = req.query.area
            var _date = getILDate (new Date())

            var params = {
                TableName: "Data",
                Key:{
                    "date": _date.getFullYear() + "_" + (_date.getMonth()+1) + "_" + _date.getDate(),
                    "region": "" + area
                }
            }

            try {
                dynamo.deleteItem(params, function(err, data) {
                    try {
                        if (err) {
                            myLog("Error: Element (" + area +"," + _date + ") not deleted. Error: " + err)
                            myLog("Tried to access table :" + params.TableName);
                        } else {
                            myLog("Element (" + area +"," + _date + ") deleted")
                        }
                    }  catch (e) {
                        myLog("Internal Exception in dynamo.deleteItem: " + e)
                    }
                });
            } catch (e) {
                    myLog("Dynamo Exception in dynamo.deleteItem: " + e)
            }
        } else {
            myLog("Missing area parameter")
        }
    });

    app.get('/local_scan_tables', (req, res) => {
        var dynamodb = new AWS.DynamoDB();
        dynamodb.listTables({}, function(err, data) {
            if (err) {
                res
                .status(500)
                .send("ERROR err=" +  JSON.stringify(err, null, 2));

            }
            else {
                res
                .status(200)
                .send("OK data=" +  JSON.stringify(data, null, 2));
            }
        });
    });

    
    app.get('/local_scan_table', (req, res) => {
        var dynamodb = new AWS.DynamoDB();
        var params = {
            TableName: 'Data',
        }        
        dynamodb.scan(params, function(err, data) {
            if (err) {
                res
                .status(500)
                .send("ERROR err=" +  JSON.stringify(err, null, 2));

            }
            else {
                res
                .status(200)
                .send("OK data=" +  JSON.stringify(data, null, 2));
            }
        });
    });

    

    app.get('/local_delete_table', (req, res) => {
        var dynamodb = new AWS.DynamoDB();
        var params = {
            TableName: 'Data',
        }        
        dynamodb.deleteTable(params, function(err, data) {
            if (err) {
                res
                .status(500)
                .send("ERROR err=" +  JSON.stringify(err, null, 2));

            }
            else {
                res
                .status(200)
                .send("OK data=" +  JSON.stringify(data, null, 2));
            }
        });
    
    });
    

    app.get('/local_create_table', (req, res) => {

        var dynamodb = new AWS.DynamoDB();

        var params = {
            TableName : "Data",
            KeySchema: [
                { AttributeName: "date", KeyType: "HASH"},  //Partition key
                { AttributeName: "region", KeyType: "RANGE" }  //Sort key
            ],
            AttributeDefinitions: [
                { AttributeName: "date", AttributeType: "S" },
                { AttributeName: "region", AttributeType: "S" }
            ],
            ProvisionedThroughput: {
                ReadCapacityUnits: 1,
                WriteCapacityUnits: 1
            }
        };

        dynamodb.createTable(params, function(err, data) {
            if (err) {

                res
                .status(500)
                .send("ERROR err=" +  JSON.stringify(err, null, 2));
            } else {
                res
                .status(200)
                .send("OK data=" +  JSON.stringify(data, null, 2));
            }
        });

    });




    // Start the server
    const PORT = process.env.PORT || 8080;
    app.listen(PORT, () => {
      console.log(`App listening on port ${PORT}`);
      console.log('Press Ctrl+C to quit.');
    });

}
